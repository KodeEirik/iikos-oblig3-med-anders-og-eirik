﻿$dir = $args[0]

$used = (Get-Volume -FilePath $dir).Size - (Get-Volume -FilePath $dir).SizeRemaining
$percent = $used / (Get-Volume -FilePath $dir).Size
Write-Output "Partisjonen $dir befinner seg på er $($percent * 100)% full"

$numberOfFiles = Get-ChildItem $dir -Recurse -File | Measure-Object | ForEach-Object{$_.Count;}
$sum = (Get-ChildItem $dir -Recurse -File | Measure-Object Length -sum).Sum
$avrg = $sum / $numberOfFiles

Write-Output "Det finnes $numberOfFiles filer"

$name = Get-ChildItem $dir -r| Sort-Object -descending -property length | Select-Object -first 1 -ExpandProperty name
$directory = Get-ChildItem $dir -r| Sort-Object -descending -property length | Select-Object -first 1 -ExpandProperty DirectoryName
$size = Get-ChildItem $dir -r| Sort-Object -descending -property length | Select-Object -first 1 -ExpandProperty Length

Write-Output "Den største er $directory\$name som er $size`B stor"
Write-Output "Gjennomsnittlig størrelse er $avrg`B"