﻿#input from user:
$arg = 0
#Do whiler until input is 9:
do {
    Write-Output "
    1 - Hvem er jeg og hva er navnet paa dette scriptet?
    2 - Hvor lenge er det siden siste boot?
    3 - Hvor mange prosesser og traader finnes?
    4 - Hvor mange context switch'er fant sted siste sekund?
    5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?
    6 - Hvor mange interrupts fant sted siste sekund?
    9 - Avslutt"
    $arg = Read-Host -Prompt 'Input your command'
switch ($arg) {
    1 {
        [Environment]::UserName
      }
    2 {
        Write-Output "Så lenge er det siden siste boot: $(Get-Uptime)".Trim()
    }
    3 {
        Write-Output "Det finnes $((Get-Process | Select-Object).Count) prosesser på maskinen".Trim()
        Write-Output "Det finnes $((Get-Process | Select-Object -ExpandProperty Threads).Count) traader på maskinen".Trim()
    }
    4 {
        $cs = $(Get-Counter -Counter "\System\Context Switches/sec")
        Write-Output "Kontekstsvitsjer siste sekund: $cs".Trim()
    }
    5 {
        $ut = $((Get-Counter -Counter "\Processor(_total)\% User Time" -SampleInterval 1 -MaxSamples 1).CounterSamples | Out-String)
        Write-Output "User time: $ut".Trim()
        $kt = $((Get-Counter -Counter "\Processor(_total)\% Privileged Time" -SampleInterval 1 -MaxSamples 1).CounterSamples | Out-String)
        Write-Output "Kernel time: $kt".Trim()
 }
    6 {
        $ils = $((Get-Counter -Counter "\Processor(_total)\Interrupts/sec").CounterSamples | Out-String)
        Write-Output "Interrupts last second: $ils".Trim()
    }
    Default {}
}
    Read-Host -Prompt 'press a key'
} while ($arg -ne 9)
