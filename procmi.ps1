﻿for ($i = 0; $i -lt $args.Count; $i++) {
    Add-Content './PID-dato.meminfo' "******** Minneinfo om prosess med PID $($args[$i]) ********"
    Add-Content './PID-dato.meminfo' "Total bruk av virtuelt minne: $((Get-Process -Id $args[$i]).VirtualMemorySize/ 1MB) MB"
    Add-Content './PID-dato.meminfo' "Størrelse på Working Set: $((Get-Process -Id $args[$i]).WorkingSet/ 1KB) KB`n`n"
}